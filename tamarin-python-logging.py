#! /usr/bin/env python -u
"""
Tamarin Execution/Logging tool for the HPC Clusters at the University of
Luxembourg
    Created by Zach Smith (ZSmith on Gitlab) Maintained by Reynaldo Gil-Pons

This script bypasses some of the issues associated with executing Tamarin on
University machines. In particular:

    - Tamarin oracles are traditionally run as python files, but on HPC
        machines it instead seems to treat them as shell scripts. A wrapper
        shell script is created which simply runs the oracle directly.

    - Tamarin's non-termination can be frustrating in scenarios where the
        script is being run remotely, since you won't know what causes it to get
        stuck in its thought process. This outputs a "rotating" log containing
        the most recent thoughts of Tamarin which can be examined after
        completion (or failure).

For more extensive instructions (i.e. how to set up Tamarin on the HPC machines
in the first place), check the git repository at 

"""

import logging
import os
import subprocess
from logging.handlers import RotatingFileHandler
import resource
import psutil
import re
import time
import argparse
from pathlib import Path


# ----------------------------------------------------------------------
# Returns a handler to a logger at the given path
def create_rotating_log(path):
    """
    Creates a rotating log
    """
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)

    # add a rotating handler
    handler = RotatingFileHandler(path, maxBytes=500000, backupCount=2)
    logger.addHandler(handler)

    return logger


# ----------------------------------------------------------------------


def find_lemmas(tamarin_file):
    lemmas = []
    with open(tamarin_file) as ff:
        p = re.compile(r"^lemma ([^\[\s:]*)(\s\[|:)?")
        for line in ff:
            m = p.match(line.strip())
            if m:
                lemmas.append(m.group(1))
    return lemmas


def setlimits():

    # memory limit
    gb = 2**30
    m = (gb * 100) * 2

    # time limit
    h = 60 * 60
    t = (h * 6) * psutil.cpu_count()

    resource.setrlimit(resource.RLIMIT_CPU, (t, t))
    resource.setrlimit(resource.RLIMIT_DATA, (m, m))


def run_main():

    parser = argparse.ArgumentParser(
        description="Helper to execute Tamarin proof for each lemma separately"
    )

    # Add positional arguments
    parser.add_argument("model", type=str, help="Tamarin model")

    # Add optional arguments (named arguments)
    parser.add_argument(
        "--prove", type=str, help="Lemmas to prove", nargs="?", const="*", required=True
    )
    parser.add_argument(
        "--derivcheck-timeout", type=int, help="Derivation check timeout", default=5
    )
    parser.add_argument(
        "--saturation",
        type=int,
        help="Limits the number of saturations during computations",
        default=5,
    )
    parser.add_argument(
        "--open-chains",
        type=int,
        help="Limits the number of open chains to be resoled during precomputations",
        default=10,
    )
    # Not supported yet, parsing the output is different
    # parser.add_argument(
    #     "--diff",
    #     help="Turn on observational equivalence mode using diff terms.",
    #     action="store_true",
    # )
    parser.add_argument(
        "--auto-sources",
        help="Try to auto-generate sources lemmas",
        action="store_true",
    )
    parser.add_argument(
        "--quit-on-warning",
        help="Strict mode that quits on any warning that is emitted.",
        action="store_true",
    )

    args = parser.parse_args()
    if args.model is None:
        print("Please call with a tamarin filename")
        return

    tamarinFilename = Path(args.model)
    if not os.path.exists(tamarinFilename):
        print("Tamarin file not found")
        return

    lemmas_prefix = args.prove

    current_unix_time = int(time.time())
    # We're banking on the tamarin file ending in .spthy
    results_log_name_root = tamarinFilename.name[:-6] + "-results"
    thoughts_log_name_root = tamarinFilename.name[:-6] + "-thoughts"

    if not os.path.exists("log"):
        os.mkdir("log")

    results_log_global = Path("log") / Path(
        f"{current_unix_time}-{results_log_name_root}.log"
    )
    if os.path.exists(results_log_global):
        os.remove(results_log_global)

    for le in find_lemmas(tamarinFilename):
        if lemmas_prefix[-1] == "*":
            if not le.startswith(lemmas_prefix[:-1]):
                continue
        elif le != lemmas_prefix:
            continue

        results_log_name = Path("log") / Path(
            f"{current_unix_time}-{results_log_name_root}-{le}.log"
        )
        thoughts_log_name = Path("log") / Path(
            f"{current_unix_time}-{thoughts_log_name_root}-{le}.log"
        )

        # Flush logs from previous execution
        if os.path.exists(thoughts_log_name):
            os.remove(thoughts_log_name)
        if os.path.exists(results_log_name):
            os.remove(results_log_name)

        processArgs = ["tamarin-prover", tamarinFilename, "--prove=" + le]
        for a in ["quit_on_warning", "auto_sources"]:
            if eval(f"args.{a}"):
                processArgs += [f"--{a.replace('_', '-')}"]
        for a in ["open_chains", "saturation", "derivcheck_timeout"]:
            v = eval(f"args.{a}")
            processArgs += [f"--{a.replace('_', '-')}={v}"]

        # This file could be very long! We only want the freshest output
        thoughts_log = create_rotating_log(thoughts_log_name)

        # This log will be a reasonable size
        results_log = open(results_log_name, "wb")

        time_start = time.time()
        proc = subprocess.Popen(
            processArgs,
            stderr=subprocess.PIPE,
            stdout=results_log,
            preexec_fn=setlimits,
        )
        mem = resource.prlimit(proc.pid, resource.RLIMIT_DATA)[1] // 2**20 // 2
        tlimit = (
            resource.prlimit(proc.pid, resource.RLIMIT_CPU)[1] // psutil.cpu_count()
        )
        msg = f"Analysing lemma: {le} Max memory: {mem}MB Max time: {tlimit}s"
        print(msg)
        with open(results_log_global, "a") as fo:
            print(msg, file=fo)

        dataenderr = False
        dataerr = b""
        while (proc.returncode is None) or (not dataenderr):
            proc.poll()
            dataenderr = False
            ndata = proc.stderr.read(2**10)
            if len(ndata) > 0:
                dataerr += ndata
                ss = dataerr.split(b"\n")
                for line in ss[:-1]:
                    thoughts_log.info(line)
                dataerr = ss[-1]
            else:
                if len(dataerr) > 0:
                    thoughts_log.info(dataerr)
                dataenderr = True

        results_log.close()
        time_elapsed = time.time() - time_start

        solved = False
        if proc.returncode == 0:
            with open(results_log_name) as fi:
                for line in fi:
                    if solved:
                        if re.match(
                            "^" + le + r" \((?:exists-trace|all-traces)\):",
                            line.strip(),
                        ):
                            msg = f"{line.strip()} Time: {round(time_elapsed, 1)}s"
                            print(msg)
                            with open(results_log_global, "a") as fo:
                                print(msg, file=fo)
                    elif line.strip() == "summary of summaries:":
                        solved = True

        if proc.returncode != 0 or not solved:

            with open(results_log_global, "a") as fo:
                msg = f"Proving {le} failed. Return code: {proc.returncode} Time: {round(time_elapsed, 1)}"
                print(msg)
                print(msg, file=fo)


if __name__ == "__main__":
    run_main()
